//
//  IntermediaryModels.swift
//  HoroscopoThinkUp
//
//  Created by Ivan Genta on 4/23/19.
//  Copyright © 2019 Ivan Genta. All rights reserved.
//

import Foundation

struct Horoscope: Codable {
    var description: String
    var luckyNumber: String
    var luckyColor: String
    var mood: String
    
    enum CodingKeys: String, CodingKey {
        case description
        case luckyNumber = "lucky_number"
        case luckyColor = "color"
        case mood
    }
}
