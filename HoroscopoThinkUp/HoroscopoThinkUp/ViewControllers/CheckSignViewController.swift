//
//  CheckSignViewController.swift
//  HoroscopoThinkUp
//
//  Created by Ivan Genta on 4/26/19.
//  Copyright © 2019 Ivan Genta. All rights reserved.
//

import UIKit
import FSCalendar

class CheckSignViewController: UIViewController, FSCalendarDataSource, FSCalendarDelegate {
    
    @IBOutlet weak var discoverSignButton: UIButton!
    var dateSelected : Date = Date()
    var rangeDatesSignsByMonth = HoroscopeManager.rangeDatesSignsByMonth
    
    var sign : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        discoverSignButton.setTitle(NSLocalizedString("Discover it!", comment: ""), for: .normal)
        // Do any additional setup after loading the view.
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        self.dateSelected = date
    }
    
    @IBAction func discoverSignButtonPressed(_ sender: Any) {
        let day = Calendar.current.component(.day, from: dateSelected)
        let month = Calendar.current.component(.month, from: dateSelected)
        self.sign = HoroscopeManager.getSign(day: day, month: month)
        
        showSignAlert()
    }
    
    /**
     Simple Alert
     */
    func showSignAlert() {
        let sign = NSLocalizedString(self.sign, comment: self.sign).capitalized
        let title = String(format: NSLocalizedString("You are %@ sign!", comment: ""), sign)
        
        let alert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
            //Cancel Action
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
}
