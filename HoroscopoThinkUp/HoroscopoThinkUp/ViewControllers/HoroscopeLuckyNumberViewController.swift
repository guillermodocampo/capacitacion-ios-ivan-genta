//
//  HoroscopeLuckyNumberViewController.swift
//  HoroscopoThinkUp
//
//  Created by Ivan Genta on 4/25/19.
//  Copyright © 2019 Ivan Genta. All rights reserved.
//

import UIKit

class HoroscopeLuckyNumberViewController: UIViewController {
    @IBOutlet weak var titleSignLabel: UILabel!
    @IBOutlet weak var luckyNumberLabel: UILabel!
    
    var sign : String!
    var horoscope : Horoscope!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initializeView()
        updateHoroscopeUI()
    }
    
    func updateHoroscopeUI() {
        let signString = NSLocalizedString(sign, comment: "")
        titleSignLabel.text = signString.capitalized
        luckyNumberLabel.text = horoscope.luckyNumber
    }

    func initializeView() {
        self.title = NSLocalizedString("luckyNumberTitle", comment: "")
    }
}
