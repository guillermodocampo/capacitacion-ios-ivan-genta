//
//  HoroscopeTableViewController.swift
//  HoroscopoThinkUp
//
//  Created by Ivan Genta on 4/23/19.
//  Copyright © 2019 Ivan Genta. All rights reserved.
//

import UIKit

class SignViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var knowSignButton: UIButton!
    
    var signs : [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signs = HoroscopeManager.signs
        
        knowSignButton.setTitle(NSLocalizedString("topInitialViewButton", comment: ""), for: .normal)
        let backItem = UIBarButtonItem()
        backItem.title = String()
        self.navigationItem.backBarButtonItem = backItem
        
    }
    
    @IBAction func knowSignButtonPressed(_ sender: Any) {
        let checkSignVC = self.storyboard?.instantiateViewController(withIdentifier: "CheckSignViewController") as! CheckSignViewController
        
        self.navigationController?.pushViewController(checkSignVC, animated: true)
    }
}

extension SignViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.signs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomSignsTableViewCellIdentifier", for: indexPath) as! CustomSignsTableViewCell
        configure(cell, forItemAt: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return NSLocalizedString("Signs", comment: "Signs")
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let optionsVC = self.storyboard?.instantiateViewController(withIdentifier: "HoroscopeOptionsViewController") as! HoroscopeOptionsViewController
        let index = indexPath.row
        let sign = signs[index]
        optionsVC.sign = sign
        self.navigationController?.pushViewController(optionsVC, animated: true)
    }
    
    func configure(_ cell: CustomSignsTableViewCell, forItemAt indexPath: IndexPath) {
        let sign = self.signs[indexPath.row]
        let title = NSLocalizedString(sign, comment: sign)
        cell.textSign?.text = title.capitalized
        cell.imageSign?.image = UIImage(named: sign)
    }
}
