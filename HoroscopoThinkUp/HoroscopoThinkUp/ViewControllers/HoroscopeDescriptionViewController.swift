//
//  HoroscopeDescriptionViewController.swift
//  HoroscopoThinkUp
//
//  Created by Ivan Genta on 4/23/19.
//  Copyright © 2019 Ivan Genta. All rights reserved.
//

import UIKit
import Moya
import NVActivityIndicatorView

class HoroscopeDescriptionViewController: UIViewController, NVActivityIndicatorViewable {
    @IBOutlet weak var titleSignLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var sign : String!
    var horoscope : Horoscope!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeView()
        updateHoroscopeUI()
    }
    
    func updateHoroscopeUI() {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let date = formatter.string(from: Date())
        
        let signString = NSLocalizedString(sign, comment: "")
        titleSignLabel.text = signString.capitalized
        dateLabel.text = NSLocalizedString("Today is", comment: "") + " \(date)"
        descriptionLabel.text = horoscope.description
    }
    
    func initializeView() {
        self.title = NSLocalizedString("dailyHoroscopeTitle", comment: "")
    }
}


