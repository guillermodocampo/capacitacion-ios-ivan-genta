//
//  HoroscopeOptionsViewController.swift
//  HoroscopoThinkUp
//
//  Created by Ivan Genta on 4/25/19.
//  Copyright © 2019 Ivan Genta. All rights reserved.
//

import UIKit
import Moya
import NVActivityIndicatorView

class HoroscopeOptionsViewController: UIViewController, NVActivityIndicatorViewable {
    @IBOutlet weak var titleSignLabel: UILabel!
    @IBOutlet weak var horoscopeButton: UIButton!
    @IBOutlet weak var luckyNroButton: UIButton!
    @IBOutlet weak var luckyColorButton: UIButton!
    @IBOutlet weak var moodButton: UIButton!
    
    var sign : String!
    let horoscopeProvider = MoyaProvider<HoroscopeRouter>()
    var horoscope : Horoscope!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeView()
        showViewLoading()
        chargeHoroscope()
        updateUI()
    }
    
    @IBAction func horoscopeButtonPressed(_ sender: Any) {
        let descriptionVC = self.storyboard?.instantiateViewController(withIdentifier: "HoroscopeDescriptionViewController") as! HoroscopeDescriptionViewController
        descriptionVC.sign = self.sign
        descriptionVC.horoscope = self.horoscope
        self.navigationController?.pushViewController(descriptionVC, animated: true)
    }
    
    @IBAction func luckyNumberButtonPressed(_ sender: Any) {
        let luckyNumberVC = self.storyboard?.instantiateViewController(withIdentifier: "HoroscopeLuckyNumberViewController") as! HoroscopeLuckyNumberViewController
        luckyNumberVC.sign = self.sign
        luckyNumberVC.horoscope = self.horoscope
        self.navigationController?.pushViewController(luckyNumberVC, animated: true)
    }
    
    @IBAction func colorLuckyButtonPressed(_ sender: Any) {
        let luckyColorVC = self.storyboard?.instantiateViewController(withIdentifier: "HoroscopeLuckyColorViewController") as! HoroscopeLuckyColorViewController
        luckyColorVC.sign = self.sign
        luckyColorVC.horoscope = self.horoscope
        self.navigationController?.pushViewController(luckyColorVC, animated: true)
    }
    
    @IBAction func moodButtonPressed(_ sender: Any) {
        let moodVC = self.storyboard?.instantiateViewController(withIdentifier: "HoroscopeMoodViewController") as! HoroscopeMoodViewController
        moodVC.sign = self.sign
        moodVC.horoscope = self.horoscope
        self.navigationController?.pushViewController(moodVC, animated: true)
    }
    
    func chargeHoroscope() {
        horoscopeProvider.request(.readHoroscope(sign: sign)) { (result) in
            switch result {
            case .success(let response) :
                let horoscope = try! JSONDecoder().decode(Horoscope.self, from: response.data)
                self.horoscope = horoscope
                self.hideViewLoading()
                break
            case .failure(let error):
                print("error request readHoroscope")
                print(error)
            }
        }
    }
    
    func showViewLoading() {
        titleSignLabel.isHidden = true
        horoscopeButton.isHidden = true
        luckyNroButton.isHidden = true
        luckyColorButton.isHidden = true
        moodButton.isHidden = true
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        startAnimating(type: .lineSpinFadeLoader)
    }
    
    func updateUI(){
        let signString = NSLocalizedString(sign, comment: "")
        titleSignLabel.text = signString.capitalized
    }
    
    func hideViewLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        stopAnimating()
        titleSignLabel.isHidden = false
        horoscopeButton.isHidden = false
        luckyNroButton.isHidden = false
        luckyColorButton.isHidden = false
        moodButton.isHidden = false
    }
    
    func initializeView() {
        self.title = NSLocalizedString("titleTopBarNavigation", comment: "")
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        self.navigationItem.backBarButtonItem = backItem
        
        setShadow(button: horoscopeButton)
        setShadow(button: luckyNroButton)
        setShadow(button: luckyColorButton)
        setShadow(button: moodButton)
    }
    
    func setShadow(button : UIButton) {
        button.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        button.layer.shadowOffset = CGSize(width: 2.0, height: 4.0)
        button.layer.shadowOpacity = 1.0
        button.layer.shadowRadius = 0.0
        button.layer.masksToBounds = false
        button.layer.backgroundColor = UIColor.white.cgColor
        button.layer.cornerRadius = 4
        button.layer.borderWidth = 0.5
    }
}
