//
//  HoroscopeMoodViewController.swift
//  HoroscopoThinkUp
//
//  Created by Ivan Genta on 4/25/19.
//  Copyright © 2019 Ivan Genta. All rights reserved.
//

import UIKit

class HoroscopeMoodViewController: UIViewController {
    @IBOutlet weak var titleSignLabel: UILabel!
    @IBOutlet weak var moodLabel: UILabel!
    
    var sign : String!
    var horoscope : Horoscope!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initializeView()
        updateHoroscopeUI()
    }
    
    func updateHoroscopeUI() {
        let signString = NSLocalizedString(sign, comment: "")
        titleSignLabel.text = signString.capitalized
        moodLabel.text = horoscope.mood
    }

    func initializeView() {
        self.title = NSLocalizedString("moodTitle", comment: "")
    }
}
