//
//  CustomSignsTableViewCell.swift
//  HoroscopoThinkUp
//
//  Created by Ivan Genta on 4/29/19.
//  Copyright © 2019 Ivan Genta. All rights reserved.
//

import UIKit

class CustomSignsTableViewCell: UITableViewCell {

    @IBOutlet weak var imageSign: UIImageView!
    @IBOutlet weak var textSign: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
