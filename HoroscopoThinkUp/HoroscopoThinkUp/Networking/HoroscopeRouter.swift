//
//  HoroscopeRouter.swift
//  HoroscopoThinkUp
//
//  Created by Ivan Genta on 4/24/19.
//  Copyright © 2019 Ivan Genta. All rights reserved.
//

import Foundation
import Moya

enum HoroscopeRouter {
    case readHoroscope(sign: String)
}

extension HoroscopeRouter: TargetType {
    var baseURL: URL {
        return URL(string: "https://aztro.sameerkumar.website")!
    }
    
    var path: String {
        switch self {
        case .readHoroscope(_):
            return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .readHoroscope(_):
            return .post
        }
    }
    
    var sampleData: Data {
        /*switch self {
         case .readHoroscope(let sign):
         return "{'sign':'\(sign)'}".data(using: .utf8)!
         }*/
        return Data()
    }
    
    var task: Task {
        switch self {
        case .readHoroscope(let sign):
            return .requestParameters(parameters: ["sign": sign, "day": "today"], encoding: URLEncoding.queryString)
        }
    }
    var headers: [String : String]? {
        return [:]//["Content-Typer": "application/json"]
    }
}
