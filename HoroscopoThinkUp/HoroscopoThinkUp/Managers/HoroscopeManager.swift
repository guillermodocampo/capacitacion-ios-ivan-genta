//
//  HoroscopeManager.swift
//  HoroscopoThinkUp
//
//  Created by Ivan Genta on 4/23/19.
//  Copyright © 2019 Ivan Genta. All rights reserved.
//

import Foundation

class HoroscopeManager {
    static let shared = HoroscopeManager()
    
    static let signs = [
        "aries",
        "taurus",
        "gemini",
        "cancer",
        "leo",
        "virgo",
        "libra",
        "scorpio",
        "sagittarius",
        "capricorn",
        "aquarius",
        "pisces"
    ]
    
    static let rangeDatesSignsByMonth : [[ Int : [String]]] = [
        [20 : ["capricorn","aquarius"]], //January
        [20 : ["aquarius","pisces"]], //February
        [21 : ["pisces","aries"]],
        [21 : ["aries","taurus"]],
        [21 : ["taurus","gemini"]],
        [22 : ["gemini","cancer"]], //June
        [23 : ["cancer","leo"]],
        [24 : ["leo","virgo"]],
        [24 : ["virgo","libra"]], //September
        [23 : ["libra","scorpio"]],
        [23 : ["scorpio","sagittarius"]],
        [22 : ["sagittarius","capricorn"]]
    ]
    
    static func getSign(day : Int, month : Int) -> String {
        let monthNumberInArray = month - 1
        let range = rangeDatesSignsByMonth[monthNumberInArray]
        let dayBeginsSign : Int = range.keys.first!
        
        var sign : String!
        let signs = range.values.first!
        if  day < dayBeginsSign   {
            sign = signs.first
        }
        else {
            sign = signs.last
        }
        
        return sign
    }
    
}
